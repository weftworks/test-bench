# Weftworks SDN Test Bench

[![pipeline status](https://gitlab.com/weftworks/test-bench/badges/staging/pipeline.svg)](https://gitlab.com/weftworks/test-bench/commits/staging)
[![coverage report](https://gitlab.com/weftworks/test-bench/badges/staging/coverage.svg)](https://gitlab.com/weftworks/test-bench/commits/staging)

## Description

A test bench for SDN controllers.
