// Weftworks SDN Solution
// Copyright (C) 2019 Antti Lohtaja
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

#include "test_suite.hpp"

namespace weftworks::test_bench::network::openflow {

namespace implementation {

request_handler::request_handler()
{ }

auto request_handler::handle_request(std::string_view data) -> boost::tribool
{
        return false;
}

} // namespace implementation

} // namespace weftworks::test_bench::network::openflow
