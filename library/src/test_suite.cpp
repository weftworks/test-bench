// Weftworks SDN Solution
// Copyright (C) 2019 Antti Lohtaja
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

#include "test_suite.hpp"

namespace weftworks::test_bench {

namespace implementation {

test_suite::test_suite(boost::asio::io_context& io_context, weftworks::cli::options& options)
        : io_context{io_context}
        , options{options}
        , of_connection{io_context}
{
        spdlog::debug("Test suite initialized.");
}

auto test_suite::run() -> bool
{
        spdlog::info("Running tests..");
        if (not run_openflow_v13()) {
                spdlog::error("One or more tests failed for OpenFlow protocol v1.3.");
                return false;
        }
        spdlog::info("All tests passed.");
        return true;
}

auto test_suite::run_openflow_v13() -> bool
{
        spdlog::info("Testing for OpenFlow protocol v1.3..");
        // TODO: tests here.

        auto handshake = false;

        auto host = options.get_value<std::string>("controller");
        auto port = options.get_value<uint16_t>("of-port");

        auto handler = [&](const boost::system::error_code& error) {
                if (error) {
                        spdlog::error("Connection error: {}.", error.message());
                } else {
                        spdlog::info("Connected to {}:{}.", host, port);
                        handshake = true;
                }
                io_context.stop();
        };

        of_connection.async_connect(host, port, handler);
        io_context.run();

        if (not handshake) {
                spdlog::error("OpenFlow v1.3 Handshake failed.");
                return false;
        }
        spdlog::info("All OpenFlow v1.3 tests passed.");
        return true;
}

} // namespace implementation

} // namespace weftworks::test_bench
