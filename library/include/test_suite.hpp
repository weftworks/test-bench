// Weftworks SDN Solution
// Copyright (C) 2019 Antti Lohtaja
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

#pragma once

#include <boost/asio.hpp>

#include <spdlog/spdlog.h>
#include <weftworks/common/cli.hpp>
#include <weftworks/common/network.hpp>

#include "network/openflow/request_handler.hpp"

namespace weftworks::test_bench {

namespace implementation {

/// run test bench tests
///
class test_suite
{
public:
        /// constructor
        ///
        /// \param io_context is a reference to a boost asio io context
        /// \param options is a reference to weftworks cli options
        test_suite(boost::asio::io_context& io_context, weftworks::cli::options& options);
        /// start running tests
        ///
        /// \return true if all tests passed
        auto run() -> bool;
private:
        /// run openflow protocol v1.3 tests
        ///
        /// \return true if tests passed
        auto run_openflow_v13() -> bool;

        boost::asio::io_context& io_context;                    ///< boost asio io context reference
        weftworks::cli::options& options;                       ///< stored cli options
        weftworks::network::openflow::connection<test_bench::network::openflow::request_handler> of_connection; ///< an openflow connection
};

} // namespace implementation

// lift the implementation
using test_suite = implementation::test_suite;

} // namespace weftworks::test_bench
