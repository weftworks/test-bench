// Weftworks SDN Solution
// Copyright (C) 2019 Antti Lohtaja
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

#pragma once

#include <string_view>

#include <boost/logic/tribool.hpp>
#include <weftworks/common/network/base/request_handler.hpp>

namespace weftworks::test_bench::network::openflow {

namespace implementation {

/// run test bench tests
///
class request_handler
        : public weftworks::network::base::request_handler
{
public:
        /// constructor
        ///
        request_handler();
        /// parse openflow messages
        ///
        auto handle_request(std::string_view data) -> boost::tribool final;
};

} // namespace implementation

// lift the implementation
using request_handler = implementation::request_handler;

} // namespace weftworks::test_bench::network::openflow
