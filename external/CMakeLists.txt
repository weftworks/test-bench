# ./external/CMakeLists.txt

add_subdirectory(boost)
add_subdirectory(catch2)
add_subdirectory(spdlog)
add_subdirectory(thread)
add_subdirectory(common_library)
