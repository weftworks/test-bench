# ./external/boost/CMakeLists.txt

# Boost version
set(BOOST_VERSION 1.68.0)

# Boost settings
set(Boost_ADDITIONAL_VERSIONS   "${BOOST_VERSION}")
set(Boost_COMPILER              "-gcc")
set(Boost_USE_MULTITHREADED     TRUE)
set(Boost_USE_STATIC_LIBS       FALSE)
set(Boost_USE_STATIC_RUNTIME    FALSE)

# Find Boost
find_package(
        Boost ${BOOST_VERSION}
        COMPONENTS
                log
                log_setup
                program_options
                system
        REQUIRED
)

# Make Boost targets globally available.
if (Boost_FOUND)
        set_target_properties(
                Boost::boost
                Boost::log
                Boost::log_setup
                Boost::program_options
                Boost::system
                PROPERTIES
                        IMPORTED_GLOBAL TRUE
        )
endif()
