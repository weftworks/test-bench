// Weftworks SDN Solution
// Copyright (C) 2019 Antti Lohtaja
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

#include <boost/asio.hpp>

#include <spdlog/spdlog.h>
#include <weftworks/common/cli.hpp>

#include "test_suite.hpp"

//#include "build_version.h"

auto main(int argc, char **argv) -> int
{
        // set and store the command line options
        auto options = weftworks::cli::options{};
        options.add<std::string>("controller,c", "controller host address");
        options.add<uint16_t>("of-port", "controller openflow port");
        options.store(argc, argv);

        // check if version option was used
        if (options.is_set("version")) {
                //std::cout << boost::format("Weftworks Test Bench version: %1%\n")
                //        % WEFTWORKS_TEST_BENCH_VERSION;
                std::cout << boost::format("Boost version: %1%.%2%.%3%\n")
                        % (BOOST_VERSION / 100000)
                        % (BOOST_VERSION / 100 % 1000)
                        % (BOOST_VERSION % 100);
                std::exit(EXIT_FAILURE);
        }

        // TODO: omitting --controller and --of-port?
        if (not options.is_set("controller") or not options.is_set("of-port")) {
                std::cout << "Usage: weftworks-test-bench -c <controller-host> -p <openflow-port>.\n";
                std::exit(EXIT_FAILURE);
        }

        spdlog::info("Weftworks SDN Test Bench - Copyright (C) 2019 Antti Lohtaja");
        spdlog::info("This program comes with ABSOLUTELY NO WARRANTY.");
        spdlog::info("This is free software, and you are welcome to redistribute");
        spdlog::info("it under certain conditions.");
        spdlog::info("");

        auto io_context = boost::asio::io_context{};
        auto suite = weftworks::test_bench::test_suite{io_context, options};

        return suite.run() ? EXIT_SUCCESS : EXIT_FAILURE;
}
